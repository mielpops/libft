NAME 				= libft.a
LIB_TEST 			= lib.so
CC 					= gcc
CFLAGS 				= -Wall -Werror -Wextra
MAKE			 	= make -C
MAKE_SUBDIR_PATH	= ./tests
SRC_DIR 			= srcs
SRCS =		$(SRC_DIR)/ft_putchar.c
SRCS +=		$(SRC_DIR)/ft_putstr.c
SRCS += 	$(SRC_DIR)/ft_strcmp.c
SRCS +=		$(SRC_DIR)/ft_strlen.c
SRCS +=		$(SRC_DIR)/ft_swap.c
SRCS +=		$(SRC_DIR)/ft_putnbr.c
SRCS +=		$(SRC_DIR)/ft_memset.c
SRCS +=		$(SRC_DIR)/ft_bzero.c
SRCS +=		$(SRC_DIR)/ft_memcpy.c
SRCS +=		$(SRC_DIR)/ft_memmove.c
SRCS +=		$(SRC_DIR)/ft_memchr.c
SRCS +=		$(SRC_DIR)/ft_memcmp.c
SRCS += 	$(SRC_DIR)/ft_strdup.c
SRCS += 	$(SRC_DIR)/ft_strndup.c
SRCS +=		$(SRC_DIR)/ft_strcpy.c
SRCS +=		$(SRC_DIR)/ft_strncpy.c
SRCS +=		$(SRC_DIR)/ft_strcat.c
SRCS +=		$(SRC_DIR)/ft_strncat.c
SRCS +=		$(SRC_DIR)/ft_strstr.c
SRCS +=		$(SRC_DIR)/ft_strchr.c
SRCS +=		$(SRC_DIR)/ft_strrchr.c
SRCS +=		$(SRC_DIR)/ft_strnstr.c
SRCS +=		$(SRC_DIR)/ft_strncmp.c
SRCS +=		$(SRC_DIR)/ft_atoi.c
SRCS +=		$(SRC_DIR)/ft_isalpha.c
SRCS +=		$(SRC_DIR)/ft_isdigit.c
SRCS +=		$(SRC_DIR)/ft_isalnum.c
SRCS +=		$(SRC_DIR)/ft_isascii.c
SRCS +=		$(SRC_DIR)/ft_isprint.c
SRCS +=		$(SRC_DIR)/ft_islower.c
SRCS +=		$(SRC_DIR)/ft_isupper.c
SRCS +=		$(SRC_DIR)/ft_tolower.c
SRCS +=		$(SRC_DIR)/ft_toupper.c
SRCS +=		$(SRC_DIR)/ft_memalloc.c
SRCS +=		$(SRC_DIR)/ft_memdel.c
SRCS +=		$(SRC_DIR)/ft_strnew.c
SRCS +=		$(SRC_DIR)/ft_strdel.c
SRCS +=		$(SRC_DIR)/ft_strclr.c
SRCS +=		$(SRC_DIR)/ft_strjoin.c
SRCS +=		$(SRC_DIR)/ft_strlcat.c
SRCS +=		$(SRC_DIR)/ft_striter.c
SRCS +=		$(SRC_DIR)/ft_memccpy.c
SRCS +=		$(SRC_DIR)/ft_striteri.c
SRCS +=		$(SRC_DIR)/ft_strmap.c
SRCS +=		$(SRC_DIR)/ft_strmapi.c
SRCS +=		$(SRC_DIR)/ft_strequ.c
SRCS +=		$(SRC_DIR)/ft_strnequ.c
SRCS +=		$(SRC_DIR)/ft_strsub.c
SRCS +=		$(SRC_DIR)/ft_strtrim.c
SRCS +=		$(SRC_DIR)/ft_strsplit.c
SRCS +=		$(SRC_DIR)/ft_itoa.c
SRCS +=		$(SRC_DIR)/ft_putendl.c
SRCS +=		$(SRC_DIR)/ft_putchar_fd.c
SRCS +=		$(SRC_DIR)/ft_putstr_fd.c
SRCS +=		$(SRC_DIR)/ft_putendl_fd.c
SRCS +=		$(SRC_DIR)/ft_putnbr_fd.c
SRCS +=		$(SRC_DIR)/ft_lstnew.c
SRCS +=		$(SRC_DIR)/ft_lstdelone.c
SRCS +=		$(SRC_DIR)/ft_lstdel.c
SRCS +=		$(SRC_DIR)/ft_lstadd.c
SRCS +=		$(SRC_DIR)/ft_lstiter.c
SRCS +=		$(SRC_DIR)/ft_lstmap.c
INCLUDES_PATH		= includes

OBJS = $(SRCS:.c=.o)
.PHONY : all clean fclean re test

all: $(NAME)

$(NAME): $(OBJS)
	@ar rc $@ $^
	@ranlib $@

%.o: %.c
	@$(CC) $(CFLAGS) -o $@ -c $^ -I $(INCLUDES_PATH)

clean:
	@rm -f $(OBJS)
	@$(MAKE) $(MAKE_SUBDIR_PATH) clean

fclean: clean
	@rm -f $(NAME)

re: fclean all

test: all
	@$(MAKE) $(MAKE_SUBDIR_PATH)
