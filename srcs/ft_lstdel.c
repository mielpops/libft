/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <tony@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/04 19:35:14 by tony              #+#    #+#             */
/*   Updated: 2019/04/04 19:44:45 by tony             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void    ft_lstdel(t_list **alst, void (*del)(void*, size_t))
{
    t_list *next_list;
    t_list *list;

    list = *alst;
    while (list)
    {
        next_list = list->next;
        del(list->content, list->content_size);
        free(list);
        list = next_list;
    }
    *alst = NULL;
}