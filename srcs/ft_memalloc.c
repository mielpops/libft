/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <tony@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/06 04:28:45 by tony              #+#    #+#             */
/*   Updated: 2019/02/06 05:00:23 by tony             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void    *ft_memalloc(size_t size)
{
    void *memory;

    memory = (void*)malloc(size);
    if (memory == NULL)
        return (NULL);
    ft_bzero(memory, size);
    return (memory);
}