/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <tony@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/25 02:39:58 by tony              #+#    #+#             */
/*   Updated: 2019/03/25 02:59:07 by tony             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void *ft_memccpy(void *restrict dst, const void *restrict src, int c, size_t n)
{
    const char *src_buf = src;

    while (n--)
    {
        if (*src_buf == (unsigned char)c)
            return dst++;
        *(unsigned char*)dst++ = *src_buf++;
    }
    return (NULL);
}