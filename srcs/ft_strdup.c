/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: toto <toto@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 11:29:23 by toto              #+#    #+#             */
/*   Updated: 2019/06/26 01:35:53 by toto             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char    *ft_strdup(const char *s1)
{
    const int len = ft_strlen(s1);
    char *dup;

    if ((dup = (char*)malloc(sizeof(char) * (len + 1))) == NULL)
        return (NULL);
    ft_memmove(dup, s1, len);
    return (dup);
}
