/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <tony@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/04 19:23:41 by tony              #+#    #+#             */
/*   Updated: 2019/06/27 09:02:20 by toto             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char    *ft_strjoin(const char *s1, const char *s2)
{
    char *copy;
    const int s1_len = ft_strlen(s1);

    copy = (char*)malloc(sizeof(char) * (s1_len + ft_strlen(s2)) + 1);
    if (copy == NULL)
        return (NULL);
    ft_strcpy(copy, s1);
    ft_strcat(copy, s2);
    return (copy);
}
