/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/22 00:41:58 by tony              #+#    #+#             */
/*   Updated: 2019/06/22 00:42:20 by tony             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char    *ft_strndup(const char *s, size_t size)
{
    const size_t    final_size = size;
    char            *ret;

    ret = (char*)malloc(sizeof(char) * size + 1);
    while (size--)
        *ret++ = *s++;
    *ret = '\0';
    return (ret - final_size);
}
