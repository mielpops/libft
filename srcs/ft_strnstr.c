/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <tony@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/04 23:05:09 by tony              #+#    #+#             */
/*   Updated: 2019/02/04 23:33:49 by tony             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char    *ft_strnstr(const char *haystack, const char *needle, size_t len)
{
    const char *pattern = needle;
    const char *src     = haystack;

    while (*haystack || len--)
    {
        needle = pattern;
        while (*needle && *src && *needle == *src++)
            needle++;
        if (*needle == '\0')
            return ((char*)haystack);
        src = ++haystack;
    }
    return (NULL);
}