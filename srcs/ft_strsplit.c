/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <tony@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 17:23:09 by toto              #+#    #+#             */
/*   Updated: 2019/04/04 14:08:56 by tony             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int  ft_count_words(const char *s, int c)
{
    int words;

    words = 0;
    while (*s)
    {
        if (*s != c && *s)
        {
            while (*s != c && *s)
                s++;
            words++;
        }
        else 
            s++;
    }
    return (words);
}

static int     ft_word_size(const char *s, int c)
{
    const char *tmp = s;

    while (*tmp != c && *tmp)
        tmp++;
    return (tmp - s);
}

static char    **ft_string_array(size_t len)
{
    char **array;

    array = (char**)malloc(sizeof(char*) * len + 1);
    if (array == NULL)
        return (NULL);
    array[len] = NULL;
    return (array);
}

char    **ft_strsplit(const char *s, char c)
{
    const int       words_count = ft_count_words(s, c);
    char            **container; 
    int             index;

    container = ft_string_array(words_count);
    while (*s)
    {
        if (*s != c && *s)
        {
            index = 0;
            if ((*container = (char*)malloc(ft_word_size(s, c) + 1)) == NULL || container == NULL)
            {
                free(container);
                return (NULL);
            }
            while (*s != c && *s)
                (*container)[index++] = *(char*)s++;
            (*container)[index] = '\0';
            container++;
        }
        else
            s++;
    }
    return (container - words_count);
}