/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <tony@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/01 21:58:32 by tony              #+#    #+#             */
/*   Updated: 2019/04/04 18:42:45 by tony             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char * ft_strtrim(char const *s)
{
    const char  *end = s;
    char        *ret;
    size_t      size_to_cpy;
    size_t      tmp_size;
    
    while (*end)
        end++;
    while (*s == ' ' || *s == '\n' || *s == '\t')
        s++;
    while (*--end == ' ' || *end == '\n' || *end == '\t');
    size_to_cpy = end - s + 1;
    tmp_size = size_to_cpy;
    ret = (char*)malloc(size_to_cpy);
    if (ret == NULL || size_to_cpy == 0)
        return (NULL);
    while (size_to_cpy--)
        *ret++ = *s++;
    ret[tmp_size] = '\0';
    return (ret - tmp_size);
}