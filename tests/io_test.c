/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   io_test.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: toto <toto@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/21 03:43:22 by toto              #+#    #+#             */
/*   Updated: 2018/10/28 02:57:59 by toto             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "unity_src/unity.h"
#include "libft.h"
#include "headers.h"
#include <stdio.h>
#include <limits.h>


static void PRINT_SHOULD_DO_WITH_STRING_PARAM(char *function_name, char *should_do, char *param)
{
    printf("%s FUNCTION SHOULD DO : \n\t %s \n", function_name, should_do);
    printf("WITH PARAMS :\n%s\n", param); 
    printf("RESULTS :\n");
}

static void PRINT_SHOULD_DO_WITH_CHAR_PARAM(char *function_name, char *should_do, char param)
{
    printf("%s FUNCTION SHOULD DO : \n\t %s \n", function_name, should_do);
    printf("WITH PARAMS :\n%c\n", param); 
    printf("RESULTS :\n");
}

static void PRINT_SHOULD_DO_WITH_INT_PARAM(char *function_name, char *should_do, int param)
{
    printf("%s FUNCTION SHOULD DO : \n\t %s \n", function_name, should_do);
    printf("WITH PARAMS :\n%i\n", param); 
    printf("RESULTS :\n");
}

static void print_underscore(void)
{
    printf("\n_____________________________________\n");
}

static void test_ft_putstr_should_print_str(void)
{
    char *params;

    params = "Hello World!";
    PRINT_SHOULD_DO_WITH_STRING_PARAM("ft_putstr", "affiche un à un les caractères d’une chaîne à l’écran.", params);
    ft_putstr(params);
    print_underscore();
}

static void test_ft_putchar_should_print_char(void)
{
    char param;

    param = 'c';

    PRINT_SHOULD_DO_WITH_CHAR_PARAM("ft_putchar", "affiche un caractère sur la sortie standard", param);
    ft_putchar(param);
    print_underscore();
}

static void test_ft_putnbr_should_print_number(void)
{
    int nbr;

    nbr = 1564;
    PRINT_SHOULD_DO_WITH_INT_PARAM("ft_putnbr", "affiche un nombre sur la sortie standard", nbr);
    ft_putnbr(nbr);
    print_underscore();

    nbr = INT_MAX;
    PRINT_SHOULD_DO_WITH_INT_PARAM("ft_putnbr", "affiche un nombre sur la sortie standard et gérer INT_MAX", nbr);
    ft_putnbr(nbr);
    print_underscore();
}

int     main(void)
{
    // Will contain I/O test
    // Printing headers
    printf("%s", io_test_header_txt);
    printf("%s", underscore_header_txt);
    printf("\n");

    // Calling I/O tests
    // Should always declare I/O test static
    test_ft_putstr_should_print_str();
    test_ft_putchar_should_print_char();
    test_ft_putnbr_should_print_number();
    return (0);
}
