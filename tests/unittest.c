#include "unity_src/unity.h"
#include "libft.h"
#include "headers.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>

# define STR_TEST "Hello World!"
# define STR_SIZE strlen(STR_TEST) + 1
# define BUFF 40
# define CPY_SIZE 6
# define MALLOC_TEST (char*)malloc(sizeof(char) * 30)
# define ARR_SIZE(arr) (sizeof(arr) / sizeof(arr[0]))


static void FT_STRCMP_SHOULD_RETURN_DIFFERENCE(void)
{
    // should return null pointer if not string
    char *s1;
    char *s2;

    s1 = "Hello world!";
    s2 = "Hella world!";
    TEST_ASSERT_EQUAL_INT(ft_strcmp(s1, s2), strcmp(s1, s2));
    s1 = "";
    s2 = "";
    TEST_ASSERT_EQUAL_INT(ft_strcmp(s1, s2), strcmp(s1, s2));
    s1 = "\0";
    s2 = "hello";
    TEST_ASSERT_EQUAL_INT(ft_strcmp(s1, s2), strcmp(s1, s2));
}

static void FT_STRLEN_SHOULD_RETURN_STRING_LENGTH(void)
{
    char *s1;

    s1 = "hello";
    TEST_ASSERT_EQUAL_INT(strlen(s1), ft_strlen(s1));
    s1 = "";
    TEST_ASSERT_EQUAL_INT(strlen(s1), ft_strlen(s1));
}

static void FT_SWAP_SHOULD_SWAP_TWO_NUMBERS(void)
{
    int nbr_1;
    int nbr_2;

    int nbr_3;
    int nbr_4;

    nbr_1 = 1;
    nbr_2 = 2;
    nbr_3 = nbr_2;
    nbr_4 = nbr_1;

    ft_swap(&nbr_1, &nbr_2);
    TEST_ASSERT_EQUAL_INT(nbr_1, nbr_3);
    TEST_ASSERT_EQUAL_INT(nbr_2, nbr_4);
}

static void FT_MEMSET_SHOULD_RETURN_BYTE_STRING_FILLED_WITH_VALUE_C_AND_FILL_MEMORY(void)
{
    char    string_1[BUFF];
    char    string_2[BUFF];
    int     c;
    size_t  len;

    strcpy(string_1, "This value has to be filled with c");
    strcpy(string_2, "This value has to be filled with c");
    c       = '@';
    len     = 5;

    TEST_ASSERT_EQUAL_STRING(ft_memset(string_1, c, len), memset(string_2, c, len));
    strcpy(string_1, "");
    strcpy(string_2, "");
    TEST_ASSERT_EQUAL_STRING(ft_memset(string_1, c, len), memset(string_2, c, len));
}

static void FT_BZERO_SHOULD_WRITE_BYTES_TO_STRING_S(void)
{
    int nbr_1[3];
    int nbr_2[3];
    int len;

    len = 3;
    nbr_1[0] = 256;
    nbr_2[0] = 256;
    for (int i = 1; i < len; i++)
    {
        nbr_1[i] = i;
        nbr_2[i] = i;
    }

    ft_bzero(nbr_1, 1);
    bzero(nbr_2, 1);
    for (int i = 0; i < len; i++)
        TEST_ASSERT_EQUAL_INT(nbr_2[i], nbr_1[i]);


    nbr_1[0] = 256;
    nbr_2[0] = 256;
    for (int i = 1; i < len; i++)
    {
        nbr_1[i] = i;
        nbr_2[i] = i;
    }
    ft_bzero(nbr_1, 1);
    bzero(nbr_2, 1);
    for (int i = 0; i < len; i++)
        TEST_ASSERT_EQUAL_INT(nbr_2[i], nbr_1[i]);
}

static void FT_MEMCPY_SHOULD_COPPY_N_BYTES_FROM_SRC_TO_DST(void)
{
    char        src_1[] = STR_TEST;
    char        src_2[] = STR_TEST;

    char        dst_1[BUFF];
    char        dst_2[BUFF];

    TEST_ASSERT_EQUAL_STRING(memcpy(dst_1, src_1, STR_SIZE), ft_memcpy(dst_2, src_2, STR_SIZE));
    for (int i = 0; i < (int)STR_SIZE - 1; i ++)
        TEST_ASSERT_EQUAL_INT8(dst_1[i], dst_2[i]);
    TEST_ASSERT_EQUAL_STRING(dst_1, dst_2);
    TEST_ASSERT_EQUAL_MEMORY(dst_1, dst_2, STR_SIZE);
}

static void FT_MEMMOVE_SHOULD_MOVE_BYTES_SAFELY_TO_DST_FROM_SRC(void)
{
    char        src_1[] = STR_TEST;
    char        src_2[] = STR_TEST;

    char        dst_1[BUFF];
    char        dst_2[BUFF];

    TEST_ASSERT_EQUAL_STRING(ft_memmove(dst_1, src_1, STR_SIZE), ft_memmove(dst_2, src_2, STR_SIZE));
    for (int i = 0; i < (int)STR_SIZE - 1; i ++)
        TEST_ASSERT_EQUAL_INT8(dst_1[i], dst_2[i]);
    TEST_ASSERT_EQUAL_STRING(dst_1, dst_2);
    TEST_ASSERT_EQUAL_MEMORY(dst_1, dst_2, STR_SIZE);   
}

static void FT_MEMCHR_SHOULD_RETURN_FIRST_OCCURENCE_OF_C(void)
{
    char        target_1    = 'W';
    char        target_2    = 'Z';

    TEST_ASSERT_EQUAL_STRING(memchr(STR_TEST, target_1, STR_SIZE), ft_memchr(STR_TEST, target_1, STR_SIZE));
    TEST_ASSERT_EQUAL_STRING(memchr(STR_TEST, target_2, STR_SIZE), ft_memchr(STR_TEST, target_2, STR_SIZE));
}

static void FT_MEMCMP_SHOULD_RETURN_DIFFERENCES_BETWEEN_S1_AND_S2_IF_THERE_IS_DIFF(void)
{
    char    s_1[]    = STR_TEST;
    char    s_2[]    = "Hella world!";

    TEST_ASSERT_EQUAL_INT(memcmp(s_1, s_2, STR_SIZE), ft_memcmp(s_1, s_2, STR_SIZE));
    strcpy(s_2, "Hello world!");
    TEST_ASSERT_EQUAL_INT(memcmp(s_1, s_2, STR_SIZE), ft_memcmp(s_1, s_2, STR_SIZE));
    strcpy(s_2, "HelloWorld!");
    TEST_ASSERT_EQUAL_INT(memcmp(s_1, s_2, STR_SIZE), ft_memcmp(s_1, s_2, STR_SIZE));
}

static void FT_STRDUP_SHOULD_DUPLICATE_S1(void)
{
    char    *s;

    s = ft_strdup(STR_TEST);
    TEST_ASSERT_EQUAL_STRING(STR_TEST, s);
    TEST_ASSERT_EQUAL_STRING(strdup(STR_TEST), ft_strdup(STR_TEST));
}

static void FT_STRCPY_SHOULD_COPY_SRC_TO_DST(void)
{
    char    s_1[STR_SIZE+1];
    char    s_2[STR_SIZE+1];

    ft_strcpy(s_1, STR_TEST);
    TEST_ASSERT_EQUAL_STRING(STR_TEST, s_1);
    TEST_ASSERT_EQUAL_STRING(strcpy(s_2, STR_TEST), ft_strcpy(s_2, STR_TEST));
}

static void FT_STRNCPY_SHOULD_COPY_N_CHARACTERS_FROM_SRC_TO_DST(void)
{
    char dst_1[STR_SIZE];
    char dst_2[STR_SIZE];

    strncpy(dst_1, STR_TEST, CPY_SIZE);
    ft_strncpy(dst_2, STR_TEST, CPY_SIZE);

    TEST_ASSERT_EQUAL_STRING(dst_1, dst_2);
}

static void FT_STRCAT_SHOULD_CONCAT_S2_TO_S1(void)
{
    char s1_1[STR_SIZE];
    char s1_2[STR_SIZE];
    const char s2[] = " World!";

    strcpy(s1_1, "Hello");
    ft_strcpy(s1_2, "Hello");

    TEST_ASSERT_EQUAL_STRING(strcat(s1_1, s2), ft_strcat(s1_2, s2));
    TEST_ASSERT_EQUAL_STRING(s1_1, s1_2);
}

static void FT_STRNCAT_SHOULD_CONCAT_N_CHAR_FROM_S2_TO_S1(void)
{
    char s1_1[STR_SIZE];
    char s1_2[STR_SIZE];
    const char s2[] = " World!";
    const size_t n = 5;

    strcpy(s1_1, "Hello");
    ft_strcpy(s1_2, "Hello");

    TEST_ASSERT_EQUAL_STRING(strncat(s1_1, s2, n), ft_strncat(s1_2, s2, n));
    TEST_ASSERT_EQUAL_STRING(s1_1, s1_2);   
}

static void FT_STRSTR_SHOULD_RETURN_POINTER_TO_NEEDLE_FROM_HAYSATCK(void)
{
    char *haystack = "haystack needle world";
    char *needle   = "needle";

    TEST_ASSERT_EQUAL_STRING(strstr(haystack, needle), ft_strstr(haystack, needle));
    needle = "";
    TEST_ASSERT_EQUAL_STRING(strstr(haystack, needle), ft_strstr(haystack, needle));
    haystack = "";
    needle = "hello";
    TEST_ASSERT_EQUAL_STRING(strstr(haystack, needle), ft_strstr(haystack, needle));
}

static void FT_STRCHR_SHOULD_RETURN_POINTER_TO_FIRST_OCCURENCE_OF_C_IN_S(void)
{
    int     c = 'c';

    TEST_ASSERT_EQUAL_PTR(strchr(STR_TEST, c), ft_strchr(STR_TEST, c));
    c = 'w';
    TEST_ASSERT_EQUAL_PTR(strchr(STR_TEST, c), ft_strchr(STR_TEST, c));
    c = ' ';
    TEST_ASSERT_EQUAL_PTR(strchr(STR_TEST, c), ft_strchr(STR_TEST, c));
    c = '\0';
    TEST_ASSERT_EQUAL_PTR(strchr(STR_TEST, c), ft_strchr(STR_TEST, c));

}

static void FT_STRRCHR_SHOULD_RETURN_POINTER_TO_LAST_OCCURENCE_OF_C_IN_S(void)
{
    char *str = STR_TEST;
    int     c = 'c';
    TEST_ASSERT_EQUAL_PTR(strrchr(str, c), ft_strrchr(str, c));
    c = 'w';
    TEST_ASSERT_EQUAL_PTR(strrchr(str, c), ft_strrchr(str, c));
    c = ' ';
    TEST_ASSERT_EQUAL_PTR(strrchr(str, c), ft_strrchr(str, c));
    c = '\0';
    TEST_ASSERT_EQUAL_PTR(strrchr(str, c), ft_strrchr(str, c));
}

static void FT_STRNSTR_SHOULD_RETURN_POINTER_TO_NEEDLE_FROM_HAYSTACK_LESS_THAN_LEN_CHARACTER(void)
{
    char *needle = "llo";
    int size = 2;

    TEST_ASSERT_EQUAL_PTR(strnstr(STR_TEST, needle, STR_SIZE), ft_strnstr(STR_TEST, needle, STR_SIZE));
    TEST_ASSERT_EQUAL_PTR(strnstr(STR_TEST, needle, size+6), ft_strnstr(STR_TEST, needle, size+6));
}

static void FT_STRNCMP_SHOULD_RETURN_DIFFERENCE_WITHIN_N_RANGE(void)
{
    char *s2 = "Hella world!";
    TEST_ASSERT_EQUAL_INT(strncmp(STR_TEST, s2, STR_SIZE), ft_strncmp(STR_TEST, s2, STR_SIZE));
    s2 = "";
    TEST_ASSERT_EQUAL_INT(strncmp(STR_TEST, s2, STR_SIZE), ft_strncmp(STR_TEST, s2, STR_SIZE));
    s2 = "hello";
    TEST_ASSERT_EQUAL_INT(strncmp(STR_TEST, s2, STR_SIZE + 3), ft_strncmp(STR_TEST, s2, STR_SIZE + 3));
}

static void FT_ATOI_SHOULD_CONVERT_NUMBER_INTO_INT_REPRESENTATION(void)
{
    // Negative number
    char *str = "    -36";
    TEST_ASSERT_EQUAL_INT(atoi(str), ft_atoi(str));
    str = "      +36";
    TEST_ASSERT_EQUAL_INT(atoi(str), ft_atoi(str));
    str = " abvs46";
    TEST_ASSERT_EQUAL_INT(atoi(str), ft_atoi(str));
}

static void FT_IS_ALPHA_SHOULD_RETURN_NON_ZERO_IF_ARGUMENT_IS_ALPHA(void)
{
    char *str = STR_TEST;

    while (*str != '\0')
    {
        TEST_ASSERT_EQUAL_INT(isalpha((unsigned char)*str), ft_isalpha((unsigned char)*str));
        str++;
    }
}

static void FT_IS_DIGIT_SHOULD_RETURN_NON_ZERO_IF_ARGUMENT_IS_DIGIT(void)
{
    char *str = "hello 5 guys";

    while (*str++ != '\0')
        TEST_ASSERT_EQUAL_INT(isdigit((unsigned char)*str), ft_isdigit((unsigned char)*str));
}

static void FT_IS_ALNUM_SHOULD_RETURN_NON_ZERO_IF_ARGUMENT_IS_ALPHA_OR_DIGIT(void)
{
    char *str = "hello 5 guys";

    while (*str++ != '\0')
        TEST_ASSERT_EQUAL_INT(isalnum((unsigned char)*str), ft_isalnum((unsigned char)*str));
}

static void FT_IS_ASCII_SHOULD_RETURN_NON_ZERO_IF_ARGUMENT_IS_ASCII(void)
{
    char *str = "hello 5 guys";

    while (*str++ != '\0')
        TEST_ASSERT_EQUAL_INT(isalnum((unsigned char)*str), ft_isalnum((unsigned char)*str));
}

static void FT_IS_PRINT_SHOULD_RETURN_NON_ZERO_IF_ARGUMENT_IS_PRINTABLE(void)
{
    char *str = "hello 5 guys";

    while (*str++ != '\0')
        TEST_ASSERT_EQUAL_INT(isalnum((unsigned char)*str), ft_isalnum((unsigned char)*str));
}

static void FT_TO_UPPER_SHOULD_RETURN_UPPER(void)
{
    char *str = "aabbbccccSA";

    while (*str++ != '\0')
        TEST_ASSERT_EQUAL_INT(toupper((unsigned char)*str), ft_toupper((unsigned char)*str));
}

static void FT_TO_LOWER_SHOULD_RETURN_LOWER(void)
{
    char *str = "FFFBBaBB";

    while (*str++ != '\0')
        TEST_ASSERT_EQUAL_INT(tolower((unsigned char)*str), ft_tolower((unsigned char)*str));
}

static void FT_MEMALLOC_ALLOCATE_MEMORY_AND_SANITIZE_IT_WITH_ZERO(void)
{
    void *TEST_1 = ft_memalloc(BUFF);
    void *TEST_2 = malloc(BUFF);

    if (TEST_2 == NULL)
        TEST_FAIL();
    ft_bzero(TEST_2, BUFF);
    TEST_ASSERT_EQUAL_MEMORY(TEST_1, TEST_2, BUFF);
}

static void FT_MEMDEL_SHOULD_FREE_AND_NULLED_POINTER_ARGUMENT(void)
{
    char *s1 = MALLOC_TEST;

    ft_memdel((void**)&s1);
    TEST_ASSERT_NULL(s1);
}

static void FT_STRNEW_SHOULD_CREATE_AND_INITIALIZE_SIZE_NEW_STR(void)
{
    int len = BUFF;
    char *s1 = ft_strnew(len);
    while (len--)
        TEST_ASSERT_EQUAL_INT('\0', *s1++);    
}

static void FT_STRDEL_SHOULD_FREE_AND_NULLED_STRING_POINTER(void)
{
    char *s1 = MALLOC_TEST;

    ft_strdel(&s1);
    TEST_ASSERT_NULL(s1);
}

static void FT_STRCLR_SHOULD_CLEAR_STR(void)
{
    char s1[] = STR_TEST;
    ft_strclr(s1);
    TEST_ASSERT_EACH_EQUAL_INT8('\0', s1, STR_SIZE);
}

static void FT_STRJOIN_SHOULD_APPEND_S1_AND_S2_AND_RETURN_A_COPY(void)
{
    TEST_ASSERT_EQUAL_STRING(STR_TEST, ft_strjoin("Hello ", "World!"));
}

static void FT_STREQU_SHOULD_RETURN_1_IF_S1_IS_EQUAL_TO_S2_OTHERWISE_0(void)
{
    TEST_ASSERT_EQUAL_INT(1, ft_strequ(STR_TEST, STR_TEST));
    TEST_ASSERT_EQUAL_INT(1, ft_strequ("", ""));
    TEST_ASSERT_EQUAL_INT(0, ft_strequ("HELLO", ""));
}

static void FT_STRNEQU_SHOULD_LEXICOGRAPHICALLY_COMPARE_N_CHAR_FROM_S1_TO_S2(void)
{
    TEST_ASSERT_EQUAL_INT(1, ft_strnequ(STR_TEST, STR_TEST, STR_SIZE-2));
    TEST_ASSERT_EQUAL_INT(0, ft_strnequ("", "", 3));
    TEST_ASSERT_EQUAL_INT(1, ft_strnequ("hella", "hello", 3));
    TEST_ASSERT_EQUAL_INT(1, ft_strnequ("hella", "hello", 0));
}

static void FT_STRSUB_RETURN_SUBSTRING_OF_S(void)
{
    TEST_ASSERT_EQUAL_STRING("lo Wo", ft_strsub(STR_TEST, 3, 5));
    TEST_ASSERT_EQUAL_STRING("Hello", ft_strsub(STR_TEST, 0, 5));
}

static void FT_STRTRIM_SHOULD_RETURN_COPY_OF_STRING_WITHOUT_BLANK_SPACES(void)
{
    TEST_ASSERT_EQUAL_STRING("hello", ft_strtrim("     hello         \n\t\n"));
    TEST_ASSERT_EQUAL_STRING("hello world", ft_strtrim("     hello world         \n\t\n"));
    TEST_ASSERT_EQUAL_STRING("hello", ft_strtrim("\n\t\nhello         \n\t\n"));
    TEST_ASSERT_EQUAL_STRING("hello", ft_strtrim("hello         \n\t\n"));
    TEST_ASSERT_EQUAL_STRING("hello", ft_strtrim("hello"));
    TEST_ASSERT_NULL(ft_strtrim(" "));
    TEST_ASSERT_NULL(ft_strtrim(""));
}

static void FT_STRSPLIT_SHOULD_SPLIT_STRING_WITH_CHAR_C_AND_RETURN_ARRAY_OF_STRING(void)
{
    char *string = "*salut*les***etudiants*";
    char **arr;
    char *to_compare[] = {"salut", "les", "etudiants"};

    arr = ft_strsplit(string, '*');
    TEST_ASSERT_EQUAL_STRING_ARRAY(to_compare, arr, 3);
    TEST_ASSERT_EQUAL_STRING("", ft_strsplit("", '*'));
}

static void FT_ITOA_SHOULD_RETURN_STRING_REPRESENTATION_OF_N(void)
{
    TEST_ASSERT_EQUAL_STRING("-2147483648", ft_itoa(-2147483648	));
    TEST_ASSERT_EQUAL_STRING("2147483647", ft_itoa(2147483647	));
    TEST_ASSERT_EQUAL_STRING("-21", ft_itoa(-21	));
    TEST_ASSERT_EQUAL_STRING("0", ft_itoa(0	));
    TEST_ASSERT_EQUAL_STRING("0", ft_itoa(-0	));
}

int     main(void) {
    // Will contain unittest
    printf("%s", unittest_header_txt);
    printf("%s", underscore_header_txt);
    printf("\n");

    UNITY_BEGIN();
    RUN_TEST(FT_STRCMP_SHOULD_RETURN_DIFFERENCE);
    RUN_TEST(FT_STRLEN_SHOULD_RETURN_STRING_LENGTH);
    RUN_TEST(FT_SWAP_SHOULD_SWAP_TWO_NUMBERS);
    RUN_TEST(FT_MEMSET_SHOULD_RETURN_BYTE_STRING_FILLED_WITH_VALUE_C_AND_FILL_MEMORY);
    RUN_TEST(FT_BZERO_SHOULD_WRITE_BYTES_TO_STRING_S);
    RUN_TEST(FT_MEMCPY_SHOULD_COPPY_N_BYTES_FROM_SRC_TO_DST);
    RUN_TEST(FT_MEMMOVE_SHOULD_MOVE_BYTES_SAFELY_TO_DST_FROM_SRC);
    RUN_TEST(FT_MEMCHR_SHOULD_RETURN_FIRST_OCCURENCE_OF_C);
    RUN_TEST(FT_MEMCMP_SHOULD_RETURN_DIFFERENCES_BETWEEN_S1_AND_S2_IF_THERE_IS_DIFF);
    RUN_TEST(FT_STRDUP_SHOULD_DUPLICATE_S1);
    RUN_TEST(FT_STRCPY_SHOULD_COPY_SRC_TO_DST);
    RUN_TEST(FT_STRNCPY_SHOULD_COPY_N_CHARACTERS_FROM_SRC_TO_DST);
    RUN_TEST(FT_STRCAT_SHOULD_CONCAT_S2_TO_S1);
    RUN_TEST(FT_STRNCAT_SHOULD_CONCAT_N_CHAR_FROM_S2_TO_S1);
    RUN_TEST(FT_STRSTR_SHOULD_RETURN_POINTER_TO_NEEDLE_FROM_HAYSATCK);
    RUN_TEST(FT_STRCHR_SHOULD_RETURN_POINTER_TO_FIRST_OCCURENCE_OF_C_IN_S);
    RUN_TEST(FT_STRRCHR_SHOULD_RETURN_POINTER_TO_LAST_OCCURENCE_OF_C_IN_S);
    RUN_TEST(FT_STRNSTR_SHOULD_RETURN_POINTER_TO_NEEDLE_FROM_HAYSTACK_LESS_THAN_LEN_CHARACTER);
    RUN_TEST(FT_STRNCMP_SHOULD_RETURN_DIFFERENCE_WITHIN_N_RANGE);
    RUN_TEST(FT_ATOI_SHOULD_CONVERT_NUMBER_INTO_INT_REPRESENTATION);
    RUN_TEST(FT_IS_ALPHA_SHOULD_RETURN_NON_ZERO_IF_ARGUMENT_IS_ALPHA);
    RUN_TEST(FT_IS_DIGIT_SHOULD_RETURN_NON_ZERO_IF_ARGUMENT_IS_DIGIT);
    RUN_TEST(FT_IS_ALNUM_SHOULD_RETURN_NON_ZERO_IF_ARGUMENT_IS_ALPHA_OR_DIGIT);
    RUN_TEST(FT_IS_ASCII_SHOULD_RETURN_NON_ZERO_IF_ARGUMENT_IS_ASCII);
    RUN_TEST(FT_IS_PRINT_SHOULD_RETURN_NON_ZERO_IF_ARGUMENT_IS_PRINTABLE);
    RUN_TEST(FT_TO_LOWER_SHOULD_RETURN_LOWER);
    RUN_TEST(FT_TO_UPPER_SHOULD_RETURN_UPPER);
    RUN_TEST(FT_MEMALLOC_ALLOCATE_MEMORY_AND_SANITIZE_IT_WITH_ZERO);
    RUN_TEST(FT_MEMDEL_SHOULD_FREE_AND_NULLED_POINTER_ARGUMENT);
    RUN_TEST(FT_STRNEW_SHOULD_CREATE_AND_INITIALIZE_SIZE_NEW_STR);
    RUN_TEST(FT_STRDEL_SHOULD_FREE_AND_NULLED_STRING_POINTER);
    RUN_TEST(FT_STRCLR_SHOULD_CLEAR_STR);
    RUN_TEST(FT_STRJOIN_SHOULD_APPEND_S1_AND_S2_AND_RETURN_A_COPY);
    RUN_TEST(FT_STREQU_SHOULD_RETURN_1_IF_S1_IS_EQUAL_TO_S2_OTHERWISE_0);
    RUN_TEST(FT_STRNEQU_SHOULD_LEXICOGRAPHICALLY_COMPARE_N_CHAR_FROM_S1_TO_S2);
    RUN_TEST(FT_STRSUB_RETURN_SUBSTRING_OF_S);
    RUN_TEST(FT_STRTRIM_SHOULD_RETURN_COPY_OF_STRING_WITHOUT_BLANK_SPACES);
    RUN_TEST(FT_STRSPLIT_SHOULD_SPLIT_STRING_WITH_CHAR_C_AND_RETURN_ARRAY_OF_STRING);
    RUN_TEST(FT_ITOA_SHOULD_RETURN_STRING_REPRESENTATION_OF_N);
    return UNITY_END();
}
